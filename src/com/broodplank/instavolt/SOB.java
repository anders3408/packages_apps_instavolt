package com.broodplank.instavolt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.stericson.RootTools.CommandCapture;
import com.stericson.RootTools.RootTools;


public class SOB extends BroadcastReceiver {

    public static final String VDD_LEVELS = "/sys/devices/system/cpu/cpu0/cpufreq/vdd_levels";
    public static final String VDD_SOB = "/sdcard/vdd_levels_sob";
    private static final String TAG = "VDDSettings";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        if (prefs.getBoolean(mainActivity.SOB_PREF, false) == false) {
            Log.i(TAG, "Restore disabled by user preference.");
            return;
        }

        CommandCapture command = new CommandCapture(0, "cat "+VDD_SOB); {
            try {

                RootTools.getShell(true).add(command).waitForFinish();


            } catch (InterruptedException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (TimeoutException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }


            try {
                BufferedReader r = new BufferedReader(new FileReader(VDD_SOB));
                StringBuilder total = new StringBuilder();
                String line;

                while((line = r.readLine()) != null) {
                    total.append(line+"\n");
                    Log.v(this.getClass().getName(), "Reading file to string."+line);

                    CommandCapture command2 = new CommandCapture(0, "busybox echo '"+line+"' > "+VDD_LEVELS); {
                        try {
                            RootTools.getShell(true).add(command2).waitForFinish();
                            Log.v(this.getClass().getName(), "busybox echo '"+line+"' > "+VDD_LEVELS);

                        } catch (InterruptedException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (IOException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (TimeoutException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                }
            } catch (IOException ex) {

            }

        }
    }
}

